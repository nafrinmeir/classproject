import unittest
from app import app

class BasicTests(unittest.TestCase):

    def setUp(self):
        # creates a test client
        self.app = app.test_client()
        # propagate the exceptions to the test client
        self.app.testing = True

    def tearDown(self):
        pass

    def test_home(self):
        # sends HTTP GET request to the application on the specified path
        result = self.app.get('/')
        # assert the response data
        self.assertEqual(result.data, b"MeirNafrin_TheKing")

if __name__ == "__main__":
    unittest.main()
