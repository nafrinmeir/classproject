argocd
kubectl port-forward svc/argocd-server -n argocd 8080:443
https://localhost:8080/

runapplication
kubectl port-forward pod/<POD_NAME> 5000:5000
http://127.0.0.1:5000/
