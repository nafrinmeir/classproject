# Use the official Python image from the Docker Hub
FROM python:3.9-alpine

# Set the working directory in the container
WORKDIR /app

# Copy the requirements.txt file into the container
COPY requirements.txt .

# Install the dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Install curl and other utilities for debugging
RUN apk update && \
    apk add --no-cache curl bash

# Print installed packages and file permissions for debugging
RUN apk info --installed > /installed_packages.txt && \
    ls -l /app > /app_permissions.txt

# Copy the rest of the application code into the container
COPY . .

# Define the command to run the application
CMD ["python", "app.py"]
